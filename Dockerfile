FROM openjdk:11.0.11-jdk AS build
WORKDIR /app

# Copy Gradle project
COPY settings.gradle .
COPY build.gradle .
COPY src src/
COPY gradle gradle/
COPY images images/
COPY gradlew .

# Build Gradlew Java project.
RUN ./gradlew clean build 

# Build runtime image
FROM openjdk:11.0.11-jre
WORKDIR /app
COPY --from=build /app/build/libs/covid19-tracker-1.0.0.jar .
EXPOSE 5000
ENTRYPOINT ["java","-jar","covid19-tracker-1.0.0.jar"]
