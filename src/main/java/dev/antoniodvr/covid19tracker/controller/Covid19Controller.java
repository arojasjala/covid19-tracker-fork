package dev.antoniodvr.covid19tracker.controller;

import dev.antoniodvr.covid19tracker.model.CountryData;
import dev.antoniodvr.covid19tracker.model.ProvinceData;
import dev.antoniodvr.covid19tracker.model.RegionData;
import dev.antoniodvr.covid19tracker.service.Covid19Service;
import dev.antoniodvr.covid19tracker.util.MathUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Controller
public class Covid19Controller {

    private Covid19Service service;

    public Covid19Controller(Covid19Service service) {
        this.service = service;
    }

    @GetMapping("/")
    public String index(Model model) {
        List<CountryData> countryData = service.getCountryData().get(380); // ISO 3166-1 numeric: 380 is Italy
        Map<Integer, List<RegionData>> regionDataGroupedByRegionCode = service.getRegionData();
        model.addAttribute("countryData", countryData);
        model.addAttribute("regionData", regionDataGroupedByRegionCode.values().stream()
                .map(regionData -> regionData.get(0))
                .sorted(Comparator.comparing(RegionData::getTotalCases).reversed())
                .collect(Collectors.toList())
        );

        List<CountryData> countryDataLastWeek = Covid19Service.getLastDaysData(countryData, 14);
        model.addAttribute("timeSeries", countryDataLastWeek.stream().map(d -> d.getDate().split("T")[0]).collect(toList()));
        model.addAttribute("totalCasesSeries", countryDataLastWeek.stream().map(CountryData::getTotalCases).collect(toList()));
        model.addAttribute("currentPositiveCasesSeries", countryDataLastWeek.stream().map(CountryData::getCurrentPositive).collect(toList()));
        model.addAttribute("currentNewPositiveCasesSeries", countryDataLastWeek.stream().map(CountryData::getCurrentNewPositive).collect(toList()));
        model.addAttribute("deathCasesSeries", countryDataLastWeek.stream().map(CountryData::getDeath).collect(toList()));
        model.addAttribute("recoveredCasesSeries", countryDataLastWeek.stream().map(CountryData::getRecovered).collect(toList()));
        model.addAttribute("intensiveCareCasesSeries", countryDataLastWeek.stream().map(CountryData::getIntensiveCare).collect(toList()));
        model.addAttribute("hospitalisedWithSymptomsCasesSeries", countryDataLastWeek.stream().map(CountryData::getHospitalisedWithSymptoms).collect(toList()));
        model.addAttribute("homeConfinementCasesSeries", countryDataLastWeek.stream().map(CountryData::getHomeConfinement).collect(toList()));
        model.addAttribute("testPerformedCasesSeries", countryDataLastWeek.stream().map(CountryData::getTestPerformed).collect(toList()));
        model.addAttribute("newPositiveByRegionLabels",
                regionDataGroupedByRegionCode.values().stream()
                        .map(regionData -> regionData.get(0).getRegionName())
                        .collect(Collectors.toList())
        );
        model.addAttribute("newPositiveByRegionSeries",
                regionDataGroupedByRegionCode.values().stream()
                        .map(regionData -> regionData.get(0).getCurrentNewPositive() > 0 ? regionData.get(0).getCurrentNewPositive() : 0)
                        .collect(Collectors.toList())
        );
        model.addAttribute("recoveredByRegionSeries",
                regionDataGroupedByRegionCode.values().stream()
                        .map(regionData -> MathUtils.diff(regionData, RegionData::getRecovered) > 0 ? MathUtils.diff(regionData, RegionData::getRecovered) : 0)
                        .collect(Collectors.toList())
        );
        model.addAttribute("deathByRegionSeries",
                regionDataGroupedByRegionCode.values().stream()
                        .map(regionData -> MathUtils.diff(regionData, RegionData::getDeath) > 0 ? MathUtils.diff(regionData, RegionData::getDeath) : 0)
                        .collect(Collectors.toList())
        );

        model.addAttribute("totalCasesPercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getTotalCases));
        model.addAttribute("currentPositivePercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getCurrentPositive));
        model.addAttribute("deathPercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getDeath));
        model.addAttribute("recoveredPercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getRecovered));
        model.addAttribute("intensiveCarePercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getIntensiveCare));
        model.addAttribute("hospitalisedWithSymptomsPercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getHospitalisedWithSymptoms));
        model.addAttribute("homeConfinementPercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getHomeConfinement));
        model.addAttribute("testPerformedPercentageChange", MathUtils.calculatePercentageChange(countryData, CountryData::getTestPerformed));

        return "index";
    }

    @GetMapping("/region/{regionCode}")
    public String region(Model model, @PathVariable Integer regionCode) {
        List<RegionData> regionData = service.getRegionData().get(regionCode);
        model.addAttribute("regionData", regionData);

        List<RegionData> regionDataLastWeek = Covid19Service.getLastDaysData(regionData, 14);
        model.addAttribute("timeSeries", regionDataLastWeek.stream().map(d -> d.getDate().split("T")[0]).collect(toList()));
        model.addAttribute("totalCasesSeries", regionDataLastWeek.stream().map(RegionData::getTotalCases).collect(toList()));
        model.addAttribute("currentPositiveCasesSeries", regionDataLastWeek.stream().map(RegionData::getCurrentPositive).collect(toList()));
        model.addAttribute("currentNewPositiveCasesSeries", regionDataLastWeek.stream().map(RegionData::getCurrentNewPositive).collect(toList()));
        model.addAttribute("deathCasesSeries", regionDataLastWeek.stream().map(RegionData::getDeath).collect(toList()));
        model.addAttribute("recoveredCasesSeries", regionDataLastWeek.stream().map(RegionData::getRecovered).collect(toList()));
        model.addAttribute("intensiveCareCasesSeries", regionDataLastWeek.stream().map(RegionData::getIntensiveCare).collect(toList()));
        model.addAttribute("hospitalisedWithSymptomsCasesSeries", regionDataLastWeek.stream().map(RegionData::getHospitalisedWithSymptoms).collect(toList()));
        model.addAttribute("homeConfinementCasesSeries", regionDataLastWeek.stream().map(RegionData::getHomeConfinement).collect(toList()));
        model.addAttribute("testPerformedCasesSeries", regionDataLastWeek.stream().map(RegionData::getTestPerformed).collect(toList()));

        Map<Integer, List<ProvinceData>> provinceDataByRegionCode = service.getProvinceData().get(regionCode).stream()
                .collect(
                        Collectors.groupingBy(
                                ProvinceData::getProvinceCode,
                                LinkedHashMap::new,
                                collectingAndThen(
                                        toList(),
                                        l -> l.stream()
                                                .sorted(Comparator.comparing(ProvinceData::getDate).reversed())
                                                .collect(toList())
                                )
                        )
                );

        model.addAttribute("totalCasesByProvinceLabels",
                provinceDataByRegionCode.values().stream()
                        .map(provinceData -> provinceData.get(0).getProvinceName())
                        .collect(toList())
        );
        model.addAttribute("totalCasesByProvinceSeries",
                provinceDataByRegionCode.values().stream()
                        .map(provinceData -> provinceData.size() == 1 ? provinceData.get(0).getTotalCases() : provinceData.get(1).getTotalCases())
                        .collect(toList())
        );
        model.addAttribute("totalNewCasesByProvinceSeries",
                provinceDataByRegionCode.values().stream()
                        .map(provinceData -> provinceData.size() == 1 ? 0 : provinceData.get(0).getTotalCases() - provinceData.get(1).getTotalCases())
                        .collect(toList())
        );

        model.addAttribute("totalCasesPercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getTotalCases));
        model.addAttribute("currentPositivePercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getCurrentPositive));
        model.addAttribute("deathPercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getDeath));
        model.addAttribute("recoveredPercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getRecovered));
        model.addAttribute("intensiveCarePercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getIntensiveCare));
        model.addAttribute("hospitalisedWithSymptomsPercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getHospitalisedWithSymptoms));
        model.addAttribute("homeConfinementPercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getHomeConfinement));
        model.addAttribute("testPerformedPercentageChange", MathUtils.calculatePercentageChange(regionData, RegionData::getTestPerformed));

        return "region";
    }

}
