<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/antoniodvr/covid19-tracker">
    <img src="images/logo.png" alt="Logo" width="96" height="96">
  </a>

  <h3 align="center">COVID19 Tracker</h3>

  <p align="center">
    A dashboard to monitoring the Coronavirus trend in Italy.
    <a href="https://github.com/antoniodvr/covid19-tracker">
    <br />
    <strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="http://www.phaeber.com/covid19/">View Demo</a>
    ·
    <a href="https://github.com/antoniodvr/covid19-tracker/issues">Report Bug</a>
    ·
    <a href="https://github.com/antoniodvr/covid19-tracker/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
* [Usage](#usage)
  * [Build an executable JAR](#build-an-executable-jar)
  * [Test the application](#test-the-application)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

[![COVID19 Tracker Screen Shot][product-screenshot]](http://www.phaeber.com/covid19/)

COVID19 Tracker is a dashboard to monitoring the Coronavirus trend in Italy.

The dashboard represent the [online dataset](https://github.com/pcm-dpc/COVID-19) provided by PCM-DPC (Presidenza del Consiglio dei Ministri -  Dipartimento della Protezione Civile).

### Built With

* [Spring Boot](https://spring.io/projects/spring-boot) 


<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

* [JDK 1.8](https://www.oracle.com/java/technologies/javase-downloads.html) or later
* [Maven 3.2+](https://maven.apache.org/download.cgi)
* A favorite text editor or IDE
* SSH access to remote docker host (from jenkins user to remote docker machine)
* Remote user must be able to run docker commands

## Usage

### Build an executable JAR

You can run the application by using:

```sh
./mvnw spring-boot:run
```

Alternatively, you can build the JAR file with:

```sh
./mvnw clean package
```

and then run the JAR file, as follows:

```sh
java -jar target/covid19-tracker-1.0.0.jar
```

### Test the application

Now that the web site is running, visit `http://localhost:5000/covid19`, where you should see the dashboard.

### How to configure SSH from jenkins to remote docker host (from jenkins user to remote docker machine)

* Run the following commands on jenkins 

```sh
sudo su jenkins
ssh-keygen (generated on /var/lib/jenkins/.ssh)
copy content from /var/lib/jenkins/.ssh/id_rsa.pub to /home/<remote user>/.ssh/authorized_keys on remote docker host
```

Test the connection from jenkins user to remote docker host

```sh
ssh user@remoteDockerHost
```

On the remote docker host
```sh
sudo usermod -aG docker $USER
```

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://github.com/antoniodvr/covid19-tracker/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Antonio Di Virgilio - [@antoniodvr](https://linkedin.com/in/antoniodvr)



## Acknowledgements

* Dataset: [PCM-DPC (Presidenza del Consiglio dei Ministri -  Dipartimento della Protezione Civile)](https://github.com/pcm-dpc/COVID-19)
* UI: [DesignRevision - Shards Dashboard Lite](https://designrevision.com/demo/shards-dashboard-lite/)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/antoniodvr/covid19-tracker.svg?style=flat-square
[contributors-url]: https://github.com/antoniodvr/covid19-tracker/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/antoniodvr/covid19-tracker.svg?style=flat-square
[forks-url]: https://github.com/antoniodvr/covid19-tracker/network/members
[stars-shield]: https://img.shields.io/github/stars/antoniodvr/covid19-tracker.svg?style=flat-square
[stars-url]: https://github.com/antoniodvr/covid19-tracker/stargazers
[issues-shield]: https://img.shields.io/github/issues/antoniodvr/covid19-tracker.svg?style=flat-square
[issues-url]: https://github.com/antoniodvr/covid19-tracker/issues
[license-shield]: https://img.shields.io/github/license/antoniodvr/covid19-tracker.svg?style=flat-square
[license-url]: https://github.com/antoniodvr/covid19-tracker/blob/master/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/antoniodvr
[product-screenshot]: images/screenshot.png
